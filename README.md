# GitLab Inc. Alfred Workflow

Handy shortcuts for GitLab Inc. team members.

## Commands

All commands are in the format `gl <command> [arguments]`.

### Basic Commands

| command   | alias   | action                                     | Allow arguments |
| -------   | ------  | ------                                     | ------          |
| `cal`     |         | Open your company calendar                 | -               |
| `hand`    | `hb`    | Open the handbook                          | Search query    |
| `meet`    | `call`  | Open the team call agenda and Zoom meeting | -               |
| `team`    |         | Open the team page                         | -               |
| `todo`    | `todos` | Open your Todos dashboard                  | -               |
| `doc`     | `d`     | Open the docs for GitLab                   | Search query    |


### Project-specific commands

| command   | alias   | action                                     |
| -------   | -----   | ------                                     |
| `about`   | `www`   | Open the [www-gitlab-com] project          |
| `ce`      |         | Open the [GitLab CE] project               |
| `ee`      |         | Open the [GitLab EE] project               |
| `gdk`     |         | Open the [GitLab Development Kit] project  |
| `inf[ra]` |         | Open the [Infrastructure] project          |
| `ob`      |         | Open the [Omnibus] project                 |
| `rb`      |         | Open the [Runbooks] project                |
| `rt`      |         | Open the [release-tools] project           |

[GitLab CE]: https://gitlab.com/gitlab-org/gitlab-ce/
[GitLab EE]: https://gitlab.com/gitlab-org/gitlab-ee/
[GitLab Development Kit]: https://gitlab.com/gitlab-org/gitlab-development-kit/
[Infrastructure]: https://gitlab.com/gitlab-com/infrastructure/
[Omnibus]: https://gitlab.com/gitlab-org/omnibus-gitlab/
[Runbooks]: https://gitlab.com/gitlab-com/runbooks
[www-gitlab-com]: https://gitlab.com/gitlab-com/www-gitlab-com/
[release-tools]: https://gitlab.com/gitlab-com/release-tools/

Project-specific commands accept arguments:

| argument   | action                   |
| -------    | ------                   |
| `#`        | open issue list          |
| `#1234`    | open issue 1234          |
| `#new`     | open a new issue         |
| `!`        | open merge request list  |
| `!1234`    | open merge request 1234  |
| `!new`     | open a new merge request |
| `@0e4cb3d` | open commit `0e4cb3d`    |

For example, `gl ce #new` will open the new issue page for the [GitLab CE]
project.

### Dashboard commands

The workflow provides shortcuts to the Issue and Merge Request dashboards.

| argument    | alias  | action                                |
| --------    | -----  | ------                                |
| `#mine`     |        | List issues _created by_ you          |
| `!mine`     |        | List merge requests _created by_ you  |
| `#assigned` | `#ass` | List issues _assigned to_ you         |
| `!assigned` | `!ass` | List merge requests _assigned to_ you |

## Variables

Some of the variables required for this workflow are private, or specific to the
individual user, and are thus cleared during export. You should set them
manually:

| variable          | description                                  |
| --------          | -----------                                  |
| `calendar_url`    | Full URL to your personal Google Calendar    |
| `team_call_id`    | Zoom ID (_not_ URL) of the Team Call meeting |
| `author_username` | Your GitLab.com username                     |
